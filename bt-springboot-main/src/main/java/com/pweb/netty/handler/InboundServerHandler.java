package com.pweb.netty.handler;

import java.nio.charset.Charset;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;

/**
 * Handles a server-side channel.
 */
@Sharable
public class InboundServerHandler extends ChannelDuplexHandler {

    private static final Logger logger = LogManager.getLogger(InboundServerHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        if (logger.isInfoEnabled()) {
            logger.info("Message Received on Server ==>> " + ((ByteBuf) msg).toString(Charset.defaultCharset()));
        }
        ctx.writeAndFlush(msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        logger.error(cause);
        ctx.channel().close();
        ctx.close();
    }
}
