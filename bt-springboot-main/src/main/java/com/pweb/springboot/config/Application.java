package com.pweb.springboot.config;

import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.pweb.netty.handler.InboundServer;

@SpringBootApplication
@ComponentScan(basePackages = { "com.pweb.springboot", "com.pweb.netty" })
public class Application {
    
    private static final Logger logger = LogManager.getLogger(Application.class);
       
    public static void main(String[] args) {

        final ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
        InboundServer inboundServer = ctx.getBean("inboundServer",InboundServer.class); 
        try {
            inboundServer.run(); 
        } catch (Exception e) { 
            logger.error("Error in Netty ==>> ",e); 
        }
        logger.info("Message ==>>> " + ctx.getMessage("dateformat", null, Locale.getDefault()));
        logger.info("name ==>> " + ctx.getMessage("name", null, Locale.getDefault()));
    }
}
