package com.pweb.springboot.webservice;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    private static final String TEMPLATE = "Hello, %s!";
    private final AtomicLong    counter  = new AtomicLong();
    private static final Logger logger   = LogManager.getLogger(HelloController.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSource dataSource;

    @RequestMapping(name = "/helloUnMarshal", produces = "application/xml")
    public Hello helloUnMarshal(@RequestParam(value = "name", defaultValue = "World") String name) {
        if (logger.isInfoEnabled()) {
            logger.info("Hello Controller Called");
        }
        try {
            final int i = jdbcTemplate.queryForObject("select count(*) from dual", Integer.class);
            logger.info("Value from Database ==>> " + i);
        } catch (final Exception e) {
            logger.error("Error in calling dual query", e);
        }
        callDisp();

        try {
            final Connection con = dataSource.getConnection();
            con.close();
        } catch (final SQLException e) {
            logger.error("Error in Fetching connection from pooling ==>> ", e);
        }

        return new Hello(counter.incrementAndGet(), String.format(TEMPLATE, name));
    }

    public void callDisp() {
        final int i = jdbcTemplate.queryForObject("select count(*) from dual", Integer.class);
        logger.info("Database calling ==>> " + i);
    }

}
