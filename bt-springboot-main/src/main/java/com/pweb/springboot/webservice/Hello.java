package com.pweb.springboot.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "XML")
@XmlAccessorType(XmlAccessType.FIELD)
public class Hello {

    @XmlElement(name = "ID")
    private final long id;

    @XmlElement(name = "CONTENT")
    private final String content;
    
    @XmlElement(name = "SERVICE")
    private String service;

    public Hello() {
        this.id = 0;
        this.content = null;
        this.service = null;
    }

    public Hello(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
