package com.pweb.springboot.integration;

import org.springframework.stereotype.Component;

@Component(value = "echoService")
public class EchoService {

    public String test(String input) {
        if ("FAIL".equals(input)) {
            throw new RuntimeException("Failure Demonstration");
        }
        return "echo:" + input;
    }
}
