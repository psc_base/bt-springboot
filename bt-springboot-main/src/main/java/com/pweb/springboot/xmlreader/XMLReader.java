package com.pweb.springboot.xmlreader;

import java.io.DataInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * XMLReader is used to read XML using JAXB and return Object
 */
@Component
public class XMLReader {

    private static final Logger logger       = LogManager.getLogger(XMLReader.class);
    private JAXBContext         jbContext    = null;
    private Unmarshaller        unMarshaller = null;

    /**
     * Class return <b>Object</b> after successful conversion from XML to Object
     * else return <b>NULL</b>
     * 
     * @param className Name of Class for XML to Object conversion
     * @param dataInputStream Data Inputstream containing XML Information
     * @return Object
     */
    public Object xmlRead(Class<?> className, DataInputStream dataInputStream) {

        try {
            jbContext = JAXBContext.newInstance(className);
            unMarshaller = jbContext.createUnmarshaller();
            return unMarshaller.unmarshal(dataInputStream);
        } catch (final JAXBException e) {
            logger.error("Error in XML Reader", e);
        }
        return null;
    }
}
